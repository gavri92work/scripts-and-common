import argparse # for parss the args
from scp import SCPClient  # sudo pip3 install --proxy=https://proxy-chain.intel.com:912 scp
import scp 
from paramiko import SSHClient
import os # for join
import progressbar # pip install progressbar2

import shutil


def get_files_to_send(Dest_PC, need_qt_dependencies, need_scripts, isLocal):
    #here you can put all the common files
    l = []

    if Dest_PC == 1:
        l.append('TestData/Qt_Json_Data/Short_Session_HilsPlayConfig.json')

        if isLocal:
            l.append("TestData/Qt_Json_Data/Short_Session_HilsConfig_localHost.json")
        else:
            l.append('TestData/Qt_Json_Data/Short_Session_HilsConfig.json')

        l.append('Session/SessionReader/libme_session_reader.so')
        l.append('build/Applications/Hils/HILS')
        l.append('build/Applications/Qt/HILS_GUI/HilsQtUI')

        #network scripts
        if need_scripts:
            l.append('scripts/Network/Network_Hils')

    else:

        if isLocal:
            l.append('TestData/Qt_Json_Data/Short_Session_AvepmEmulatorConfig_localHost.json')
        else:
            l.append('TestData/Qt_Json_Data/Short_Session_AvepmEmulatorConfig.json')
            l.append('TestData/Short_Session_AvepmEmulatorConfigDpdk.json')
        
        l.append('build/Applications/AvepmEmulator/AvepmEmulator')
        l.append('build/Applications/Qt/AvepmEmulator_GUI/AvepmQtUI')

        #network scripts
        if need_scripts:
            l.append('scripts/Network/Network_Avepm')

    # for qt dependence's
    if need_qt_dependencies:
        l.append('scripts/BuildDistribution/qtd')
        l.append('scripts/BuildDistribution/setupQt.sh')
    
    return l

def get_destination_and_host(Dest_PC):
    if Dest_PC == 1:
        return "/home/hils/Desktop", "10.155.133.52"
    else:
        return "/home/hils-car-pc-2/Desktop/" , "10.155.133.54"



# SSH/SCP Directory Recursively     
def ssh_scp_files(ssh_host, ssh_user, ssh_password, ssh_port, source_volume, destination_volume):
    # logging.info("In ssh_scp_files()method, to copy the files to the server")
    ssh = SSHClient()
    ssh.load_system_host_keys()
    ssh.connect(ssh_host, username=ssh_user, password=ssh_password, look_for_keys=False)

    with SCPClient(ssh.get_transport()) as scp:
        scp.put(source_volume, recursive=True, remote_path=destination_volume)

def sendFIleOverSSH(fileRelativePath, destination, host_name):
    try:
        ssh_scp_files(ssh_host = host_name ,ssh_user="hils", ssh_password="hils", ssh_port=1,
            source_volume=fileRelativePath ,destination_volume=destination )
    except FileNotFoundError as error:
        print(error)
    except scp.SCPException as error:
        print(error)
    except Exception as e:
        print(e)



def main():
    parser = argparse.ArgumentParser()
  
    parser.add_argument("Dest_PC", type=int, choices=range(1, 3), help="Destination PC? Hils server (1) or Avepm Emulator (2)")
    parser.add_argument('Dest_Folder',type=str, help="Destination Folder? any folder that you want in selected PC, That exist in Desktop")

    parser.add_argument("-v", "--verbose", help="increase output verbosity", action="store_true")
    parser.add_argument("-l", "--local", help="increase output verbosity", action="store_true")
    parser.add_argument("-q", "--qt_dependencies", help="if need to copy the Qt dependencies", action="store_true")
    parser.add_argument("-s", "--scripts", help="if need to copy the network scripts", action="store_true")


    args = parser.parse_args()

    if args.verbose:
        print("verbosity turned on")
        print(args)
        print()

    if args.local:
        print("send local")
        files = get_files_to_send(args.Dest_PC, args.qt_dependencies, args.scripts, args.local)
        with progressbar.ProgressBar(max_value=len(files), redirect_stdout=True) as bar:
            for i, file in enumerate(files):
                if os.path.isfile(file):
                    if args.verbose:
                        print("Sending: {:65} ---> To:{:25}".format(file, args.Dest_Folder))
                    shutil.copy(file, args.Dest_Folder)
                    bar.update(i)
        return;


    files = get_files_to_send(args.Dest_PC, args.qt_dependencies, args.scripts, args.local)
    des_home_path, hostname = get_destination_and_host(args.Dest_PC)
    destination_path = os.path.join(des_home_path, args.Dest_Folder)

    with progressbar.ProgressBar(max_value=len(files), redirect_stdout=True) as bar:
        for i, file in enumerate(files):
            if args.verbose:
                print("Sending: {:65} ---> To: {:12}:{:25}".format(file, hostname, destination_path))
            sendFIleOverSSH(fileRelativePath = file, destination = destination_path, host_name = hostname ) 
            bar.update(i)



if __name__ == '__main__':
    main()
