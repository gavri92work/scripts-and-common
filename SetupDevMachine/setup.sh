#!/bin/bash

echo Ubuntu Setup script ~Hils~

# for disable case sensetive
echo set completion-ignore-case on | sudo tee -a /etc/inputrc
# reload bash

# update the APT
sudo apt-get update -y -q
sudo apt-get upgrade -y -q

# install ssh and restart 
sudo apt-get install ssh -y -q
sudo systemctl restart ssh

#copy alias file
file=./.alias
if [ -e "$file" ]; then
	echo "copy alias to home"
	cp .alias ~/.alias
	source ~/.alias > /dev/null 2>&1
else 
	echo "alias File does not exist"
fi 
echo 'source ~/.alias' | sudo tee -a ~/.bashrc


#open vscode and add proxy in user setting and close vscode in order to run this commands
#install vscode extensions
code --install-extension aaron-bond.better-comments
code --install-extension IBM.output-colorizer

code --install-extension ms-python.python
code --install-extension Tyriar.terminal-tabs
code --install-extension zhuangtongfa.Material-theme
code --install-extension TzachOvadia.todo-list
code --install-extension PKief.material-icon-theme
code --install-extension bbenoist.QML
code --install-extension eamodio.gitlens
code --install-extension streetsidesoftware.code-spell-checker
code --install-extension vector-of-bool.cmake-tools
code --install-extension twxs.cmake
code --install-extension ms-vscode.cpptools


# gnome tweak tool
sudo apt install gnome-tweak-tool -y -q
sudo apt install xclip -y -q

sudo apt install cmake -y -q
sudo apt install g++ -y -q
sudo apt install python3-pip -y -q


#zipping
sudo apt install zip unzip

#monitoring
sudo apt install bmon
sudo apt install nmon


#network tools
sudo apt install net-tools

echo Ubuntu Setup Finish ~!!!!~







