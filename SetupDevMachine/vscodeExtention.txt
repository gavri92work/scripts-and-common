
Installation
Launch VS Code Quick Open (Ctrl+P), paste the following command, and press enter.

code --install-extension aaron-bond.better-comments
code --install-extension IBM.output-colorizer

code --install-extension ms-python.python
code --install-extension Tyriar.terminal-tabs
code --install-extension zhuangtongfa.Material-theme
code --install-extension TzachOvadia.todo-list
code --install-extension PKief.material-icon-theme
code --install-extension bbenoist.QML
code --install-extension eamodio.gitlens
code --install-extension streetsidesoftware.code-spell-checker
code --install-extension vector-of-bool.cmake-tools
code --install-extension twxs.cmake
code --install-extension ms-vscode.cpptools
